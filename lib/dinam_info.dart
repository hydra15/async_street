// show inputbox with int value
import 'package:async_street/geoStreet.dart';
import 'package:flutter/material.dart';

class DinamInfo extends StatefulWidget {
  const DinamInfo({super.key, required this.latitud, required this.longitud});

  final double latitud;
  final double longitud;

  @override
  State<DinamInfo> createState() => _DinamInfoState();
}

class _DinamInfoState extends State<DinamInfo> {
  // inputbox receive dinamyc street hint
  final TextEditingController _streetController = TextEditingController();
  final String street = 'Street';

  @override
  void initState() {
    super.initState();
    _streetController.text = street;
    // call findAddressesFromLatLng params latitud and longitud and print result, and set street
    Future<String> awaitStreet = GeoStreet.findAddressesFromLatLng(
      widget.latitud,
      widget.longitud,
    );
    awaitStreet.then((value) {
      setState(() {
        _streetController.text = value;
        print('Street: $value');
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dinamyc Info'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Street:',
            ),
            TextField(
              controller: _streetController,
            ),
          ],
        ),
      ),
    );
  }
}
