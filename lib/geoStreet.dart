// class and method findAddressesFromQuery by latitude and longitude
import 'package:geocoder/geocoder.dart';

class GeoStreet {
  // method findAddressesFromQuery by latitude and longitude, return string
  static Future<String> findAddressesFromLatLng(
      double latitude, double longitude) async {
    List<Address> addresses =
        await Geocoder.local.findAddressesFromQuery('$latitude, $longitude');
    // get street
    String asyncStreet = addresses.first.addressLine;
    if (asyncStreet == null) {
      asyncStreet = 'Default Street';
    } else {
      asyncStreet = addresses.first.addressLine;
    }

    print('asyncStreet: $asyncStreet');

    return asyncStreet;
  }
}
