// button for sent lat/long to DinamInfo
import 'package:async_street/dinam_info.dart';
import 'package:async_street/geoStreet.dart';
import 'package:flutter/material.dart';

class StaticOptions extends StatelessWidget {
  const StaticOptions({super.key});

  @override
  Widget build(BuildContext context) {
    // rLatitud and rLongitud, -17.7834925,-63.1821515
    late double rLatitud = -17.7834925;
    late double rLongitud = -63.1821515;
    late String rStreet = 'Street';

    return Scaffold(
      appBar: AppBar(
        title: const Text('Static Options'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            // show rLatitud and rLongitud
            Text(
              'Latitud: $rLatitud',
              style: Theme.of(context).textTheme.headline4,
            ),
            Text(
              'Longitud: $rLongitud',
              style: Theme.of(context).textTheme.headline4,
            ),
            // street text default
            Text(
              'Street: $rStreet',
              style: Theme.of(context).textTheme.headline4,
            ),
            // button for sent lat/long to DinamInfo
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DinamInfo(
                      latitud: rLatitud,
                      longitud: rLongitud,
                    ),
                  ),
                );
              },
              child: const Text('Send Lat/Long'),
            ),
            // button to call findAddressesFromLatLng and print result
            ElevatedButton(
              onPressed: () {
                // call findAddressesFromLatLng params rLatitud and rLongitud
                // and print result
                Future<String> awaitStreet = GeoStreet.findAddressesFromLatLng(
                  rLatitud,
                  rLongitud,
                );
                awaitStreet.then((value) {
                  print('awaitStreet: $value');
                  rStreet = value;
                });
              },
              child: const Text('Call findAddressesFromLatLng'),
            ),
          ],
        ),
      ),
    );
  }
}
